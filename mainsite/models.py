from django.db import models

from users.models import CustomUser

# Create your models here.
class Ingredient(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    kcal = models.IntegerField(null=True)
    carbs = models.FloatField(null=True)
    protein = models.FloatField(null=True)
    fat = models.FloatField(null=True)

    AMOUNT_CHOICES = [
        ('g', 'g'),
        ('cl', 'cl'),
        ('cup', 'cup'),
        ('tsp', 'tsp'),
        ('tbsp', 'tbsp'),
        ('piece', 'piece'),
    ]

    amount = models.FloatField(default=0.0)
    measurement = models.CharField(max_length=5, choices=AMOUNT_CHOICES, default='g')

    def __str__(self):
        return self.name


class Recipe(models.Model):
    name = models.CharField(max_length=100)
    pub_date = models.DateField()
    author = models.ForeignKey(CustomUser, on_delete=models.DO_NOTHING)

    ingredients = models.ManyToManyField(Ingredient)

    def __str__(self):
        return self.name