# Generated by Django 3.0.2 on 2020-01-28 17:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainsite', '0002_auto_20200128_1840'),
    ]

    operations = [
        migrations.AddField(
            model_name='ingredient',
            name='amount',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='ingredient',
            name='measurement',
            field=models.CharField(choices=[('g', 'g'), ('cl', 'cl'), ('cup', 'cup'), ('tsp', 'tsp'), ('tbsp', 'tbsp'), ('piece', 'piece')], default='g', max_length=5),
        ),
    ]
