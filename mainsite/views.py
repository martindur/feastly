from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView

from .forms import IngredientForm, RecipeForm


class IndexView(TemplateView):
    template_name = 'mainsite/index.html'


class IngredientCreateView(CreateView):
    form_class = IngredientForm
    success_url = reverse_lazy('profile')
    template_name = 'mainsite/ingredient_create.html'


class RecipeCreateView(CreateView):
    form_class = RecipeForm
    success_url = reverse_lazy('profile')
    template_name = 'mainsite/recipe_create.html'