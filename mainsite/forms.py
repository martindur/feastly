from django import forms
from .models import Ingredient, Recipe


class IngredientForm(forms.ModelForm):
    AMOUNT_CHOICES = [
        ('g', 'g'),
        ('cl', 'cl'),
        ('cup', 'cup'),
        ('tsp', 'tsp'),
        ('tbsp', 'tbsp'),
        ('piece', 'piece'),
    ]
    measurement = forms.CharField(max_length=5, widget=forms.Select(choices=AMOUNT_CHOICES))
    class Meta:
        model = Ingredient
        fields = ('name', 'kcal', 'carbs', 'protein', 'fat', 'amount', 'measurement')


class RecipeForm(forms.ModelForm):
    ingredients = forms.ModelMultipleChoiceField(queryset=Ingredient.objects.all())
    class Meta:
        model = Recipe
        fields = ('name', 'ingredients')