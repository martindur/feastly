from django.urls import path

from .views import IndexView, IngredientCreateView, RecipeCreateView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('ingredient_new/', IngredientCreateView.as_view(), name='ingredient_new'),
    path('recipe_new/', RecipeCreateView.as_view(), name='recipe_new'),
]