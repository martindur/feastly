from django.urls import path
from .views import RegisterView, ProfileView

urlpatterns = [
    path('signup/', RegisterView.as_view(), name='signup'),
    path('profile/', ProfileView.as_view(), name='profile')
]